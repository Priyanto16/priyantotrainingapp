import React, {useEffect, useState} from 'react';
import MapboxGL from '@react-native-mapbox-gl/maps';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  SafeAreaView,
  FlatList,
} from 'react-native';
import Axios from 'axios';
import api from '../../api';
import colors from '../../style/colors';
import ProgressBar from 'react-native-animated-progress';
import moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Index = ({navigation}) => {
  const [riwayat, setRiwayat] = useState({});
  const [loading, setLoading] = useState(false);

  const getRiwayat = (token) => {
    Axios.get(`${api}/donasi/riwayat-transaksi`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
      .then((res) => {
        if (res.data.response_code == '00') {
          setRiwayat(res.data.data.riwayat_transaksi);
        }
        console.log('donasi -> res', res);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };

  useEffect(() => {
    navigation.setOptions({title: 'Riwayat Transaksi'});
    getToken = async () => {
      setLoading(true);

      try {
        const token = await AsyncStorage.getItem('token', token);
        console.log('profile -> token', token);
        return getRiwayat(token);
      } catch (err) {
        console.log(err);
      }
    };
    getToken();
  }, []);
  const formatMoney = (val) => {
    let format = val.toLocaleString('id-ID', {
      style: 'currency',
      currency: 'IDR',
    });
    return format;
  };
  const renderItem = ({item}) => (
    <View style={styles.itemContainer}>
      <View style={styles.textContainer}>
        <Text style={styles.txtTotal}>Total</Text>
        <Text style={styles.txtTotal}>{formatMoney(item.amount)}</Text>
      </View>
      <View style={styles.textContainer}>
        <Text>Donation ID</Text>
        <Text>{item.order_id}</Text>
      </View>
      <View style={styles.textContainer}>
        <Text>Tanggal Transaksi</Text>
        <Text>{moment(item.created_at).format('DD/MM/YYYY H:mm')}</Text>
      </View>
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
      {loading && (
        <ProgressBar indeterminate backgroundColor={colors.deeppink} />
      )}
      <FlatList
        data={riwayat}
        removeClippedSubviews={true}
        initialNumToRender={10}
        renderItem={renderItem}
        keyExtractor={(item) => String(item.id)}
        showsHorizontalScrollIndicator={false}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 0,
    backgroundColor: colors.whitesmoke,
  },
  chart: {
    flex: 1,
  },
  itemContainer: {backgroundColor: colors.white, padding: 10, marginBottom: 10},
  scrollView: {
    backgroundColor: 'whitesmoke',
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  content: {
    padding: 0,
  },
  txtTotal: {fontWeight: 'bold'},
});

export default Index;
