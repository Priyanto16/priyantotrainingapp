import React, {useEffect, useState, useContext, useRef} from 'react';
import {
  Image,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  TouchableOpacity,
  Modal,
  Dimensions,
  Text,
  TextInput,
  ToastAndroid,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/Feather';
import api from '../../api';
import {base_url} from '../../api';
import colors from '../../style/colors';
import {SafeAreaView} from 'react-native-safe-area-context';
import {TextInputMask} from 'react-native-masked-text';
import ProgressBar from 'react-native-animated-progress';
import Button from '../../components/Button';
import InputError from '../../components/InputError';
const {height, width} = Dimensions.get('window');

const index = ({navigation}) => {
  const camera = useRef(null);
  const moneyField = useRef(null);

  const [isVisible, setIsVisible] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);
  const [token, setToken] = useState('');
  const [state, setState] = useState({
    title: '',
    amount: '',
    description: '',
  });

  const [validation, setValidation] = useState({
    title: true,
    amount: true,
    description: true,
  });

  useEffect(() => {
    navigation.setOptions({title: 'Buat Donasi'});

    getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token', token);
        setToken(token);
        console.log('profile -> token', token);
      } catch (err) {
        console.log(err);
      }
    };

    getToken();
  }, []);

  const togleCamera = () => {
    setType(type == 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    if (camera) {
      const options = {quality: 0.5, base64: true};
      const data = await camera.current.takePictureAsync(options);
      console.log(data.uri);
      setPhoto(data.uri);
      setIsVisible(false);
    }
  };

  const onSave = () => {
    if (
      state.title.length > 0 &&
      state.description.length > 0 &&
      state.amount.length > 0
    ) {
      setLoading(true);
      const formData = new FormData();
      formData.append('title', state.title);
      formData.append('description', state.description);
      formData.append('donation', String(moneyField.current.getRawValue()));
      if (photo) {
        formData.append('photo', {
          uri: photo,
          name: 'photo.jpg',
          type: 'image/jpg',
        });
      }
      console.log(token, formData);

      Axios.post(`${api}/donasi/tambah-donasi`, formData, {
        timeout: 20000,
        headers: {
          Authorization: 'Bearer ' + token,
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
        },
      })
        .then((res) => {
          if (res.data.response_code == '00') {
            ToastAndroid.show(
              'Donasi Baru berhasil ditambahkan',
              ToastAndroid.SHORT,
            );

            navigation.goBack();
          } else {
            ToastAndroid.show(
              'Terjadi kesalahan, coba lagi nanti',
              ToastAndroid.SHORT,
            );
            setLoading(false);
          }
          console.log('craete donation -> res', res);
        })
        .catch((err) => {
          setLoading(false);
          ToastAndroid.show(
            'Terjadi kesalahan, coba lagi nanti',
            ToastAndroid.SHORT,
          );
          console.log(err);
        });
    } else {
      setValidation({
        description: state.description ? true : false,
        title: state.title ? true : false,
        amount: state.amount ? true : false,
      });
    }
  };

  const ImageHeader = () => (
    <View style={styles.imageContainer}>
      {photo ? (
        <Image
          resizeMethod="resize"
          resizeMode="cover"
          source={{uri: photo}}
          style={styles.image}
        />
      ) : (
        <TouchableOpacity
          style={styles.iconContainer}
          onPress={() => setIsVisible(true)}>
          <Icon name={'camera'} size={22} color={colors.lighter} />
          <Text style={{color: colors.white}}>Pilih gambar</Text>
        </TouchableOpacity>
      )}
      {photo && (
        <TouchableOpacity
          onPress={() => setPhoto(null)}
          style={{position: 'absolute', top: 10, right: 10}}>
          <Icon name={'trash'} size={22} color={colors.lighter} />
        </TouchableOpacity>
      )}
    </View>
  );

  const Divider = (props) => <View style={{height: props.height}} />;

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1}}
            type={type}
            ref={camera}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}>
            <View style={styles.shapeBotom}>
              <TouchableOpacity
                style={styles.cameraIcon}
                onPress={() => takePicture()}>
                <Icon name={'camera'} size={33} color={colors.dark} />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.switcCamera}
                onPress={() => togleCamera()}>
                <Icon name={'refresh-cw'} size={22} color={colors.dark} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="deepskyblue" />
      {isLoading && (
        <ProgressBar indeterminate backgroundColor={colors.deeppink} />
      )}
      <SafeAreaView style={styles.container}>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.content}>
            <ImageHeader />
            <TextInput
              placeholder={'Judul'}
              style={styles.txtInput}
              onChangeText={(text) => {
                setState({...state, title: text});
                setValidation({
                  ...validation,
                  title: text.length > 0 ? true : false,
                });
              }}
              value={state.title}
            />
            {!validation.title && <InputError text={'Masukan judul.'} />}
            <TextInput
              multiline={true}
              numberOfLines={5}
              textAlignVertical={'top'}
              placeholder={'Deskripsi'}
              style={[styles.txtInput, {height: 100}]}
              onChangeText={(text) => {
                setState({...state, description: text});
                setValidation({
                  ...validation,
                  description: text.length > 0 ? true : false,
                });
              }}
              value={state.description}
            />
            {!validation.description && (
              <InputError text={'Masukan deskripsi.'} />
            )}

            <TextInputMask
              type={'money'}
              ref={moneyField}
              value={state.amount}
              options={{
                precision: 0,
                separator: ',',
                delimiter: '.',
                unit: 'Rp. ',
                suffixUnit: '',
              }}
              placeholder={'Rp. 0'}
              onChangeText={(value) => {
                console.log(value);
                setState({...state, amount: value});
                setValidation({
                  ...validation,
                  amount: value != 'Rp. 0' ? true : false,
                });
              }}
            />
            {!validation.amount && (
              <InputError text={'Masukan nominal donasi.'} />
            )}

            <Divider height={15} />
            <Button
              isLoading={isLoading}
              title="BUAT"
              color={colors.primary}
              onPress={() => onSave()}
            />
          </View>
        </ScrollView>
        {renderCamera()}
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
    backgroundColor: colors.white,
  },
  content: {
    paddingHorizontal: 20,
  },
  txtInput: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    flex: 1,
    height: 50,
  },
  item: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  imageContainer: {
    padding: 10,
    alignItems: 'center',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignSelf: 'center',
    width: width,
    height: 200,
    backgroundColor: 'grey',
  },
  itemLabel: {
    marginLeft: 20,
  },
  name: {
    marginLeft: 10,
    fontWeight: 'bold',
  },
  navTitle: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  itemPrice: {
    position: 'absolute',
    right: 10,
  },
  image: {
    height: 200,
    width: width,
  },
  navBar: {
    paddingVertical: 10,
    backgroundColor: 'deepskyblue',
  },
  line: {flex: 1, height: 1},
  iconContainer: {
    width: width,
    backgroundColor: 'grey',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 33,
  },
  switcCamera: {
    height: 60,
    width: 60,
    borderRadius: 50,
    backgroundColor: colors.white,
    position: 'absolute',
    right: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  camerBtnContainer: {
    borderColor: colors.palegoldenrod,
    borderWidth: 2,
    height: 150,
    width: width - 100,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  cameraIcon: {
    height: 80,
    width: 80,
    borderRadius: 50,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  shapeContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  shapeTop: {
    borderWidth: 2,
    borderColor: colors.palegoldenrod,
    height: 300,
    width: 200,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 20,
  },
  shapeBotom: {
    width: width,
    position: 'absolute',
    bottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default index;
