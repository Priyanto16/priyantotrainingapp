import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import colors from '../style/colors';

const InputError = ({text}) => {
  return (
    <View>
      <Text style={styles.text}>{text}</Text>
    </View>
  );
};

export default InputError;

const styles = StyleSheet.create({
  text: {color: colors.deeppink},
});
