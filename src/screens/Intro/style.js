import {StyleSheet, Dimensions} from 'react-native';
import colors from '../../style/colors';
const {height, width} = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    padding: 10,
  },
  textLogoContainer: {
    alignItems: 'center',
    marginTop: 10,
  },
  slider: {
    flex: 1,
    backgroundColor: colors.white,
  },
  imgList: {height: height / 2, width: width},
  textLogo: {
    color: colors.primary,
    fontWeight: 'bold',
    fontSize: 22,
  },
  listContainer: {
    flex: 1,
    alignItems: 'center',
  },
  listContent: {
    alignItems: 'center',
  },
  blueText: {
    color: 'blue',
  },
  redText: {
    color: 'red',
  },
  activeDotStyle: {
    backgroundColor: colors.primary,
    width: 20,
  },
  textList: {
    textAlign: 'center',
    color: colors.primary,
    fontSize: 16,
  },
  btnContainer: {
    height: 130,
    padding: 10,
  },
});
