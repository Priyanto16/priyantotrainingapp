import React, {useEffect, useState, useContext} from 'react';
import {
  Image,
  StyleSheet,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
  Dimensions,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import api from '../../api';
import {host} from '../../api';
import {RootContext} from '../../../App';
import colors from '../../style/colors';
import ProgressBar from 'react-native-animated-progress';
const {height, width} = Dimensions.get('window');

const Items = ({item, onPress}) => {
  let format = item?.donation.toLocaleString('id-ID', {
    style: 'currency',
    currency: 'IDR',
  });
  return (
    <TouchableOpacity style={[styles.carditem]} onPress={() => onPress(item)}>
      <Image
        resizeMode="cover"
        resizeMethod="resize"
        style={styles.imgCard}
        source={
          item.photo
            ? {uri: `${host}${item.photo}`}
            : require('../../assets/images/no-image.png')
        }
      />
      <View style={styles.cardcontent}>
        <Text style={styles.title} numberOfLines={2}>
          {item.title}
        </Text>
        <Text style={styles.name}>{item.user?.name}</Text>
        <View
          style={{
            paddingVertical: 10,
          }}>
          <ProgressBar
            useNativeDriver={true}
            progress={Math.random() * (100 - 0) + 0}
            height={7}
            backgroundColor={colors.deeppink}
            animated={true}
          />
        </View>
        <Text style={styles.name}>Dana yang dibutuhkan</Text>
        <Text style={styles.money}>{format}</Text>
      </View>
    </TouchableOpacity>
  );
};
const index = ({navigation}) => {
  const {state, setUserData, loginType} = useContext(RootContext);
  const [donasi, setDonasi] = useState({});
  const [loading, setLoading] = useState(false);

  const getDonasi = (token) => {
    Axios.get(`${api}/donasi/daftar-donasi`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
      .then((res) => {
        if (res.data.response_code == '00') {
          setDonasi(res.data.data.donasi?.reverse());
        }
        console.log('donasi -> res', res);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };

  const handleBtn = (item) => {
    navigation.navigate('DetailDonasi', {data: item});
  };

  useEffect(() => {
    getToken = async () => {
      setLoading(true);

      try {
        const token = await AsyncStorage.getItem('token', token);
        console.log('profile -> token', token);
        return getDonasi(token);
      } catch (err) {
        console.log(err);
      }
    };
    getToken();
  }, []);

  const renderItem = ({item, index}) => (
    <Items item={item} index={index} onPress={handleBtn} />
  );

  return (
    <SafeAreaView>
      <StatusBar barStyle="light-content" backgroundColor="deepskyblue" />
      {loading && (
        <ProgressBar indeterminate backgroundColor={colors.deeppink} />
      )}
      <FlatList
        data={donasi}
        removeClippedSubviews={true}
        initialNumToRender={10}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        showsHorizontalScrollIndicator={false}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'white',
  },
  content: {
    padding: 0,
  },
  item: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  itemLabel: {
    marginLeft: 20,
  },
  txtsaldoTitle: {marginLeft: 10, color: colors.white},
  name: {
    marginLeft: 10,
    fontWeight: 'bold',
  },
  navTitle: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  itemPrice: {
    position: 'absolute',
    right: 10,
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 40,
  },
  txtSaldo: {color: colors.white},
  navBar: {
    paddingVertical: 10,
    backgroundColor: 'deepskyblue',
  },
  line: {flex: 1, height: 1},
  headerContainer: {
    flex: 1,
    padding: 10,
    backgroundColor: colors.primary,
    justifyContent: 'flex-start',
    height: 100,
  },
  textInput: {
    paddingHorizontal: 10,
    height: 40,
    borderColor: colors.white,
    borderWidth: 1,
    flex: 1,
    marginHorizontal: 40,
    borderRadius: 10,
    color: colors.white,
  },
  headerContent: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  card: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    borderRadius: 10,
    backgroundColor: colors.white,
    flex: 1,
    elevation: 5,
    marginTop: -40,
    marginHorizontal: 10,
  },
  btnContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    borderRadius: 10,
    backgroundColor: colors.white,
    flex: 1,
    marginHorizontal: 10,
  },
  btnMoney: {
    backgroundColor: colors.primary,
    padding: 10,
    borderRadius: 5,
  },

  carditem: {
    backgroundColor: colors.white,
    marginVertical: 8,
    marginTop: 0,
    overflow: 'hidden',
    elevation: 5,
    flexDirection: 'row',
    padding: 10,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  name: {
    fontSize: 14,
  },
  money: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  btnImage: {justifyContent: 'center', alignItems: 'center'},
  imgCard: {height: 150, width: '50%', borderRadius: 10},
  cardcontent: {paddingLeft: 10, width: '50%'},
  imageComponentStyle: {
    borderRadius: 15,
    width: '95%',
    marginVertical: 5,
    marginHorizontal: 20,
    backgroundColor: colors.white,
  },
  iconSaldo: {flexDirection: 'row', alignItems: 'center'},
  image: {height: 30, width: 30},
  btnImageSize: {height: 40, width: 40},
});

export default index;
