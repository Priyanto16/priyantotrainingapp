import React from 'react';
import {StyleSheet, Text, View, ActivityIndicator, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import colors from '../style/colors';

const Button = ({onPress, isLoading, color, title, withIcon, type}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPress} style={styles.Btn(color)}>
        {withIcon && (
          <Image
            source={
              title == 'GOOGLE'
                ? require('../assets/icons/google.png')
                : require('../assets/icons/fingerprint.png')
            }
            style={styles.img}
          />
        )}

        <Text style={styles.title(type)}>{title}</Text>
        <View style={styles.loadingContainer}>
          {isLoading && <ActivityIndicator size="small" color={colors.white} />}
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  Btn: (color) => ({
    elevation: 5,
    height: 50,
    backgroundColor: color,
    alignItems: 'center',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  }),
  img: {
    height: 20,
    width: 20,
    marginHorizontal: 10,
  },
  loadingContainer: {marginLeft: 10},
  title: (type) => ({
    color: type == 'secondary' ? colors.primary : colors.white,
  }),
});
