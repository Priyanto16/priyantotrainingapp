import React, {useState, useEffect, useContext} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  Text,
  ToastAndroid,
} from 'react-native';
import Axios from 'axios';
import Separator from '../../components/Separator';
import colors from '../../style/colors';
import api from '../../api';
import {RootContext} from '../../../App';
import {TouchableOpacity} from 'react-native-gesture-handler';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Button from '../../components/Button';
const InputOTP = ({route, navigation}) => {
  const [code, setCode] = useState('');
  const {name, email} = route.params;
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    navigation.setOptions({title: 'Daftar'});
  }, []);

  const onVerifyPress = () => {
    let data = {
      otp: code,
    };
    Axios.post(`${api}/auth/verification`, data, {
      timeout: 20000,
    })
      .then((res) => {
        console.log('verification -> res', res);
        let data = res.data;
        if (data.response_code == '00') {
          navigation.navigate('SetupPassword', {email: email});
          ToastAndroid.show(data.response_message, ToastAndroid.SHORT);
        } else if (data.response_code == '01') {
          ToastAndroid.show(data.response_message, ToastAndroid.SHORT);
        } else {
          ToastAndroid.show('Terjadi kesalahan', ToastAndroid.SHORT);
        }
      })
      .catch((err) => {
        ToastAndroid.show('Terjadi kesalahan', ToastAndroid.SHORT);
        console.log(err);
      });
  };

  const onresendOTP = () => {
    setLoading(true);
    let data = {
      email: email,
    };
    Axios.post(`${api}/auth/regenerate-otp`, data, {
      timeout: 20000,
    })
      .then((res) => {
        console.log('verification -> res', res);
        let data = res.data;
        if (data.response_code) {
          ToastAndroid.show(data.response_message, ToastAndroid.LONG);
        } else {
          ToastAndroid.show('verifikasi gagal!', ToastAndroid.LONG);
        }
        setLoading(false);
      })
      .catch((err) => {
        ToastAndroid.show('verifikasi gagal!', ToastAndroid.LONG);
        console.log(err);
        setLoading(false);
      });
  };

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="deepskyblue" />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}>
        <View style={styles.content}>
          <Text style={styles.txtgreting}>
            Perjalanan baikmu dimaulai disini!
          </Text>
          <Separator />

          <Text style={styles.txtEmailContainer}>
            Masukan email yang dikirim ke
            <Text style={styles.txtemail}> {email}</Text>
          </Text>
          <Separator />
          <OTPInputView
            style={{width: '100%', height: 100}}
            pinCount={6}
            code={code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
            onCodeChanged={(code) => {
              setCode(code);
            }}
            autoFocusOnLoad
            codeInputFieldStyle={styles.underlineStyleBase}
            codeInputHighlightStyle={styles.underlineStyleHighLighted}
            onCodeFilled={(code) => {
              console.log(`Code is ${code}, you are good to go!`);
            }}
          />
          <Button
            isLoading={isLoading}
            title="VERIFIKASI"
            color={colors.primary}
            onPress={() => onVerifyPress()}
          />
          <View style={styles.txtOrContainer}>
            <Text style={styles.txtOr}>Belum menerima kode verifikasi?</Text>
            <TouchableOpacity onPress={() => onresendOTP()}>
              <Text style={styles.txtLogin}>Kirim Ulang</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'whitesmoke',
  },
  content: {
    padding: 20,
  },
  item: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  itemLabel: {
    marginLeft: 20,
  },
  name: {
    marginLeft: 10,
    fontWeight: 'bold',
  },
  navTitle: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  itemPrice: {
    position: 'absolute',
    right: 10,
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 20,
  },
  navBar: {
    paddingVertical: 10,
    backgroundColor: 'deepskyblue',
  },
  line: {flex: 1, height: 1},
  txtInput: {height: 40, borderBottomWidth: StyleSheet.hairlineWidth},
  txtOr: {alignSelf: 'center', color: colors.dark},
  txtOrContainer: {
    alignItems: 'center',
    padding: 10,
    justifyContent: 'center',
  },
  txtLogin: {
    color: colors.primary,
    fontWeight: 'bold',
    marginLeft: 10,
  },
  txtgreting: {
    color: colors.black,
    fontWeight: 'bold',
  },
  txtemail: {
    color: colors.black,
    fontWeight: 'bold',
  },
  txtEmailContainer: {
    color: colors.black,
  },
  borderStyleBase: {
    width: 30,
    height: 45,
  },

  borderStyleHighLighted: {
    borderColor: colors.primary,
    color: colors.black,
    fontWeight: 'bold',
  },

  underlineStyleBase: {
    color: colors.black,
    fontWeight: 'bold',
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
    borderColor: colors.dark,
  },

  underlineStyleHighLighted: {
    color: colors.black,
    fontWeight: 'bold',
    borderColor: colors.primary,
  },
});

export default InputOTP;
