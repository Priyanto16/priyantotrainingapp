import React, {useEffect, useState, useContext} from 'react';
import {
  Image,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/Feather';
import api from '../../api';
import {base_url} from '../../api';
import {GoogleSignin} from '@react-native-community/google-signin';
import {CommonActions} from '@react-navigation/native';
import {RootContext} from '../../../App';
import colors from '../../style/colors';

const index = ({navigation}) => {
  const {state, setUserData, loginType} = useContext(RootContext);
  const price = '120.000.000';
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [image, setImage] = useState('');
  const [userInfo, setUserInfo] = useState(null);

  const getProfile = (token) => {
    Axios.get(`${api}/profile/get-profile`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
      .then((res) => {
        console.log('profile -> res', res);
        let data = res.data.data.profile;
        setName(data.name);
        setEmail(data.email);
        setImage(base_url + data.photo);
        console.log('image', base_url + data.photo);
        setUserData(data.name, data.email, `${base_url + data.photo}`);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const getCurrentUser = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const usrInfo = await GoogleSignin.signInSilently();
      console.log('usrInfo', usrInfo);
      if (usrInfo) {
        setUserInfo(usrInfo);
        setUserData(usrInfo.user.name, usrInfo.user.email, usrInfo.user.photo);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleLogOut = async () => {
    switch (loginType) {
      case 'google':
        try {
          await GoogleSignin.revokeAccess();
          await GoogleSignin.signOut();
          await AsyncStorage.removeItem('token');
          await AsyncStorage.setItem('isLogin', 'false');
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{name: 'SignoutNavigator'}],
            }),
          );
        } catch (err) {
          console.log(err);
          await AsyncStorage.removeItem('token');
          await AsyncStorage.setItem('isLogin', 'false');
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{name: 'SignoutNavigator'}],
            }),
          );
        }
        break;

      default:
        try {
          await AsyncStorage.removeItem('token');
          await AsyncStorage.setItem('isLogin', 'false');
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{name: 'SignoutNavigator'}],
            }),
          );
        } catch (err) {
          console.log(err);
        }
        break;
    }
  };

  const onLogoutPress = (name) => {
    switch (name) {
      case 'Keluar':
        handleLogOut();
        break;
      case 'Bantuan':
        navigation.navigate('Maps');
        break;

      default:
        break;
    }
  };

  const configureGoogleSignin = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '932606465253-i5i8dt885vdn5s2jhlk409t7o44pf3c4.apps.googleusercontent.com',
    });
  };

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      console.log('profile -> token', token);
      return getProfile(token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    console.log('login type => ', loginType);
    if (loginType == 'google') {
      configureGoogleSignin();

      getCurrentUser();
    } else if (loginType == 'default') {
      getToken();
    }
  }, []);

  const Header = () => (
    <TouchableOpacity
      onPress={() => navigation.navigate('Account')}
      style={styles.profileContainer}>
      <Image
        resizeMethod="resize"
        resizeMode="cover"
        source={
          image
            ? {
                uri: image + '?' + new Date(),
                cache: 'reload',
                headers: {Pragma: 'no-cache'},
              }
            : userInfo
            ? {uri: userInfo.user.photo}
            : require('../../assets/images/no_image.png')
        }
        style={styles.avatar}
      />
      <View>
        <Text style={styles.name}>{state ? state.name : name}</Text>
        <Text style={styles.email}>{state ? state.email : email}</Text>
      </View>
    </TouchableOpacity>
  );

  const Item = (props) => (
    <TouchableOpacity
      style={styles.item}
      onPress={() => onLogoutPress(props.name)}>
      <Icon name={props.icon} size={22} color={'grey'} />
      <Text style={styles.itemLabel}>{props.name}</Text>
      <Text style={styles.itemPrice}>{props.price}</Text>
    </TouchableOpacity>
  );

  const Divider = (props) => <View style={{height: props.height}} />;
  const Line = () => <View style={styles.line} />;

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="deepskyblue" />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}>
        <View style={styles.content}>
          <Header />
          <Line />
          <Item name="Saldo" icon="credit-card" price={price} />
          <Divider height={5} />
          <Item name="Pengaturan" icon="settings" />
          <Line />
          <Item name="Bantuan" icon="help-circle" />
          <Line />
          <Item name="Syarat & Ketentuan" icon="clipboard" />
          <Divider height={5} />
          <Item name="Keluar" icon="log-out" />
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'whitesmoke',
  },
  content: {
    padding: 0,
  },
  item: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  profileContainer: {
    backgroundColor: colors.primary,
    padding: 10,
    paddingBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemLabel: {
    marginLeft: 20,
  },
  name: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: colors.white,
  },
  email: {
    marginLeft: 10,
    color: colors.white,
  },
  navTitle: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  itemPrice: {
    position: 'absolute',
    right: 10,
  },
  avatar: {
    height: 60,
    width: 60,
    borderRadius: 60,
    backgroundColor: colors.lighter,
  },
  navBar: {
    paddingVertical: 10,
    backgroundColor: 'deepskyblue',
  },
  line: {flex: 1, height: 1},
});

export default index;
