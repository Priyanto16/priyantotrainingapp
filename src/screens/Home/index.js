import React, {useEffect, useState, useContext} from 'react';
import {
  Image,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  FlatList,
  Dimensions,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/Feather';
import api from '../../api';
import {base_url} from '../../api';
import {GoogleSignin} from '@react-native-community/google-signin';
import {RootContext} from '../../../App';
import colors from '../../style/colors';
import {SliderBox} from 'react-native-image-slider-box';
const {height, width} = Dimensions.get('window');
const Items = ({title, index, img}) => (
  <View style={[styles.carditem, {marginLeft: index == 0 ? 10 : 0}]}>
    <Image resizeMode="cover" style={styles.imgCard} source={{uri: img}} />
    <View style={styles.cardcontent}>
      <Text style={styles.title}>{title}</Text>
    </View>
  </View>
);
const index = ({navigation}) => {
  const {state, setUserData, loginType} = useContext(RootContext);
  const price = '120.000.000';
  const saldo = '0';
  const [name, setName] = useState('');
  const [image, setImage] = useState('');
  const [userInfo, setUserInfo] = useState(null);
  const [searchtext, setSearchText] = useState('');
  const images = [
    'https://source.unsplash.com/1024x768/?nature',
    'https://source.unsplash.com/1024x768/?water',
    'https://source.unsplash.com/1024x768/?girl',
    'https://source.unsplash.com/1024x768/?tree',
  ];

  const DATA = [
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      title: 'First Item',
      img: 'https://source.unsplash.com/1024x768/?nature',
    },
    {
      id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
      title: 'Second Item',
      img: 'https://source.unsplash.com/1024x768/?nature',
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d72',
      title: 'Third Item',
      img: 'https://source.unsplash.com/1024x768/?nature',
    },
  ];
  const getProfile = (token) => {
    Axios.get(`${api}/profile/get-profile`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
      .then((res) => {
        console.log('profile -> res', res);
        let data = res.data.data.profile;
        setName(data.name);
        setImage(base_url + data.photo);
        console.log('image', base_url + data.photo);
        setUserData(data.name, data.email, `${base_url + data.photo}`);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleBtn = (name) => {
    switch (name) {
      case 'Donasi':
        navigation.navigate('Donasi');
        break;
      case 'Statistik':
        navigation.navigate('Charts');
        break;
      case 'Riwayat':
        navigation.navigate('Transaksi');
        break;
      case 'Bantu':
        navigation.navigate('CreateDonation');
        break;

      default:
        break;
    }
  };

  const configureGoogleSignin = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '932606465253-i5i8dt885vdn5s2jhlk409t7o44pf3c4.apps.googleusercontent.com',
    });
  };

  const getCurrentUser = async () => {
    try {
      const usrInfo = await GoogleSignin.signInSilently();
      console.log('usrInfo', usrInfo);
      if (usrInfo) {
        setUserInfo(usrInfo);
        setUserData(usrInfo.user.name, usrInfo.user.email, usrInfo.user.photo);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token', token);
        console.log('profile -> token', token);
        return getProfile(token);
      } catch (err) {
        console.log(err);
      }
    };
    getToken();
    if (loginType == 'goole') {
      configureGoogleSignin();
      getCurrentUser();
    } else if (loginType == 'default') {
      getToken();
    }
  }, []);

  const Header = () => (
    <View style={styles.headerContainer}>
      <View style={styles.headerContent}>
        <Image
          source={require('../../assets/icons/give-love.png')}
          style={styles.image}
          resizeMode="cover"
          resizeMethod="resize"
        />
        <TextInput
          placeholder={'cari disini'}
          placeholderTextColor={colors.white}
          style={styles.textInput}
          onChangeText={(text) => setSearchText(text)}
          value={searchtext}
        />
        <Icon name={'heart'} size={30} color={colors.white} />
      </View>
    </View>
  );

  const BtnIcon = (props) => (
    <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <Icon name={props.icon} size={22} color={colors.black} />
      <Text style={{}}>{props.name}</Text>
    </View>
  );
  const BtnImage = (props) => (
    <TouchableOpacity
      style={styles.btnImage}
      onPress={() => handleBtn(props.name)}>
      <View style={styles.imgContainer}>
        <Image
          source={props.img}
          style={styles.btnImageSize}
          resizeMethod="resize"
          resizeMode="cover"
        />
      </View>
      <Text style={styles.txtBtn}>{props.name}</Text>
    </TouchableOpacity>
  );

  const BtnMoney = (props) => (
    <View style={styles.btnMoney}>
      <View style={styles.iconSaldo}>
        <Icon name={props.icon} size={18} color={colors.orange} />
        <Text style={styles.txtsaldoTitle}>Saldo</Text>
      </View>
      <Text style={styles.txtSaldo}>Rp. {saldo}</Text>
    </View>
  );

  const Divider = (props) => <View style={{height: props.height}} />;
  const renderItem = ({item, index}) => (
    <Items title={item.title} index={index} img={item.img} />
  );

  return (
    <SafeAreaView>
      <StatusBar barStyle="light-content" backgroundColor="deepskyblue" />
      <View style={styles.bg} />
      <Header />

      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}>
        <View style={styles.content}>
          <View style={styles.card}>
            <BtnMoney icon="credit-card" />
            <BtnIcon name="Isi" icon="plus-square" />
            <BtnIcon name="Riwayat" icon="refresh-cw" />
            <BtnIcon name="Lainya" icon="grid" />
          </View>
          <Divider height={10} />

          <SliderBox
            images={images}
            disableOnPress
            dotColor={colors.primary}
            inactiveDotColor={colors.dark}
            ImageComponentStyle={styles.imageComponentStyle}
            imageLoadingColor={colors.primary}
            autoplay
          />

          <Divider height={5} />
          <View style={styles.btnContainer}>
            <BtnImage
              name="Donasi"
              img={require('../../assets/icons/cheque.png')}
            />
            <BtnImage
              name="Statistik"
              img={require('../../assets/icons/crowdfunding.png')}
            />
            <BtnImage
              name="Riwayat"
              img={require('../../assets/icons/icon-statistik.png')}
            />
            <BtnImage
              name="Bantu"
              img={require('../../assets/icons/online-donation.png')}
            />
          </View>
          <Divider height={5} />

          <View style={{flex: 1}}>
            <Text style={{fontWeight: 'bold', marginHorizontal: 10}}>
              Penggalangan Dana Mendesak
            </Text>
            <FlatList
              data={DATA}
              horizontal
              renderItem={renderItem}
              keyExtractor={(item) => item.id}
              showsHorizontalScrollIndicator={false}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  scrollView: {},
  content: {
    padding: 0,
    paddingBottom: 60,
  },
  item: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  itemLabel: {
    marginLeft: 20,
  },
  txtsaldoTitle: {marginLeft: 10, color: colors.white},
  name: {
    marginLeft: 10,
    fontWeight: 'bold',
  },
  navTitle: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  itemPrice: {
    position: 'absolute',
    right: 10,
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 40,
  },
  txtSaldo: {color: colors.white},
  navBar: {
    paddingVertical: 10,
    backgroundColor: 'deepskyblue',
  },
  line: {flex: 1, height: 1},
  headerContainer: {
    padding: 10,
    backgroundColor: colors.primary,
    justifyContent: 'flex-start',
  },
  textInput: {
    paddingHorizontal: 10,
    height: 40,
    borderColor: colors.white,
    borderWidth: 1,
    flex: 1,
    marginHorizontal: 40,
    borderRadius: 10,
    color: colors.white,
  },
  headerContent: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  card: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    borderRadius: 10,
    backgroundColor: colors.white,
    flex: 1,
    elevation: 5,
    marginTop: 10,
    marginHorizontal: 10,
  },
  btnContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    borderRadius: 10,
    backgroundColor: colors.white,
    flex: 1,
    marginHorizontal: 10,
  },
  btnMoney: {
    backgroundColor: colors.primary,
    padding: 10,
    borderRadius: 5,
  },
  bg: {
    width: width,
    height: 110,
    backgroundColor: colors.primary,
    position: 'absolute',
    top: 10,
  },
  txtBtn: {
    marginTop: 10,
    color: colors.primary,
  },
  carditem: {
    backgroundColor: colors.white,
    marginVertical: 8,
    marginRight: 10,
    width: width - 100,
    marginTop: 10,
    borderRadius: 15,
    overflow: 'hidden',
    elevation: 5,
  },
  title: {
    fontSize: 14,
  },
  btnImage: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgCard: {height: 150},
  cardcontent: {padding: 10},
  imageComponentStyle: {
    borderRadius: 15,
    width: '95%',
    marginVertical: 5,
    marginHorizontal: 20,
    backgroundColor: colors.white,
  },
  iconSaldo: {flexDirection: 'row', alignItems: 'center'},
  image: {height: 30, width: 30},
  btnImageSize: {height: 35, width: 35},
  imgContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 50,
    backgroundColor: colors.primary,
    borderRadius: 15,
  },
});

export default index;
