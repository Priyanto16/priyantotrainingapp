import React, {useEffect, useState, useContext, useRef} from 'react';
import {
  Image,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
  Dimensions,
  Modal,
  ToastAndroid,
  ActivityIndicator,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/Feather';
import api from '../../api';
import {host} from '../../api';
import {RootContext} from '../../../App';
import colors from '../../style/colors';
import {TextInputMask} from 'react-native-masked-text';
import Button from '../../components/Button';
import {color} from 'react-native-reanimated';
const {height, width} = Dimensions.get('window');

const Items = ({item, index, selected, onPress}) => {
  let format = item.amount.toLocaleString('id-ID', {
    style: 'currency',
    currency: 'IDR',
  });
  return (
    <TouchableOpacity
      onPress={() => onPress(item.amount)}
      style={[
        styles.btnAmount,
        {
          backgroundColor:
            item.amount == selected ? colors.deeppink : colors.white,
        },
      ]}>
      <Text
        style={{
          color: item.amount == selected ? colors.white : colors.deeppink,
        }}>
        {format}
      </Text>
    </TouchableOpacity>
  );
};
const DetailDonasi = ({navigation, route}) => {
  const {state, setUserData, loginType} = useContext(RootContext);
  const [loading, setLoading] = useState(false);
  const [token, setToken] = useState('');
  const [data, setData] = useState(route.params.data);
  const [modalVisible, setModalVisible] = useState(false);
  const [donateAmount, setDonate] = useState(0);
  const [total, setTotal] = useState(0);
  const moneyField = useRef(null);

  const DATA = [
    {id: 1, amount: 10000},
    {id: 2, amount: 20000},
    {id: 3, amount: 50000},
    {id: 4, amount: 100000},
    {id: 5, amount: 200000},
    {id: 6, amount: 500000},
  ];

  const formatMoney = (amount) => {
    return amount.toLocaleString('id-ID', {
      style: 'currency',
      currency: 'IDR',
    });
  };

  const handlePay = () => {
    setLoading(true);
    console.log(donateAmount);

    if (donateAmount == 0) {
      ToastAndroid.show('Pilih Nominal', ToastAndroid.SHORT);
      setLoading(false);
      return;
    }

    const time = new Date().getTime();
    let body = {
      transaction_details: {
        order_id: `order-${time}`,
        gross_amount: donateAmount,
        donation_id: data.id,
      },
      customer_details: {
        first_name: state.name,
        email: state.email,
      },
    };

    Axios.post(`${api}/donasi/generate-midtrans`, body, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
      .then((res) => {
        if (res.data.response_code == '00') {
          navigation.navigate('Payments', {data: res.data.data});
        }
        console.log('donasi -> res', res);
        setLoading(false);
        setModalVisible(false);
        setDonate(0);
        setTotal(formatMoney(0));
      })
      .catch((err) => {
        setLoading(false);
        setModalVisible(false);
        setDonate(0);
        setTotal(formatMoney(0));
        console.log(err);
      });
  };

  const handleSelect = (amount) => {
    setDonate(amount);
    setTotal(formatMoney(amount));
  };

  useEffect(() => {
    navigation.setOptions({title: 'Detail Donasi'});

    getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token', token);
        console.log('profile -> token', token);
        setToken(token);
      } catch (err) {
        console.log(err);
      }
    };
    getToken();
  }, []);

  const renderItem = ({item, index}) => (
    <Items
      item={item}
      index={index}
      selected={donateAmount}
      onPress={handleSelect}
    />
  );

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
      <ScrollView>
        <View style={styles.headerContainer}>
          <Image
            resizeMode="cover"
            resizeMethod="resize"
            style={styles.imgCard}
            source={
              data.photo
                ? {uri: `${host}${data.photo}`}
                : require('../../assets/images/no-image.png')
            }
          />
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={styles.iconback}>
            <Icon name={'arrow-left'} size={22} color={colors.white} />
          </TouchableOpacity>
        </View>
        <View
          style={{
            padding: 10,
            elevation: 5,
            backgroundColor: colors.white,
          }}>
          <Text style={styles.title}>{data.title}</Text>
          <Text style={styles.desc}>
            Dana yang dibutuhkan {formatMoney(data.donation)}
          </Text>
          <Text style={styles.money}>Deskripsi</Text>
          <Text style={styles.desc}>{data.description}</Text>
          <Button
            title="DONASI SEKARANG"
            color={colors.primary}
            onPress={() => setModalVisible(true)}
          />
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.title}>Informasi Penggalang Dana</Text>
          <View style={styles.card}>
            <Text style={styles.desc}>Penggalang Dana</Text>
            <View style={styles.carditem}>
              <View style={styles.iconUser}>
                <Image
                  resizeMode="cover"
                  resizeMethod="resize"
                  style={styles.imgAvatar}
                  source={
                    data.user?.photo
                      ? {uri: `${host}${data.user.photo}`}
                      : require('../../assets/images/no-image.png')
                  }
                />
              </View>
              <Text style={styles.title}>{data.user?.name}</Text>
            </View>
          </View>
        </View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(false);
          }}>
          <StatusBar
            animated
            barStyle="light-content"
            backgroundColor={colors.transparent}
          />

          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <Text style={styles.modalText}>ISI NOMINAL</Text>
                  <TouchableOpacity onPress={() => setModalVisible(false)}>
                    <Icon name={'x'} size={22} color={colors.black} />
                  </TouchableOpacity>
                </View>

                <TextInputMask
                  type={'money'}
                  ref={moneyField}
                  value={total}
                  options={{
                    precision: 2,
                    separator: ',',
                    delimiter: '.',
                    unit: 'Rp. ',
                    suffixUnit: '',
                  }}
                  placeholder={'Rp. 0'}
                  onChangeText={(value) => {
                    setTotal(value);
                    setDonate(moneyField.current.getRawValue() * 10);
                  }}
                />
                <FlatList
                  data={DATA}
                  numColumns={2}
                  columnWrapperStyle={{justifyContent: 'space-between'}}
                  removeClippedSubviews={true}
                  initialNumToRender={10}
                  renderItem={renderItem}
                  keyExtractor={(item) => item.id}
                />
                <View style={{marginVertical: 10}} />
              </View>
              <TouchableOpacity
                style={styles.btnLanjut}
                onPress={() => handlePay()}>
                <Text style={styles.txtLanjut}>LANJUT</Text>
                <View style={styles.loadingContainer}>
                  {loading && (
                    <ActivityIndicator size="small" color={colors.white} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'white',
  },
  content: {
    padding: 0,
  },
  item: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  itemLabel: {
    marginLeft: 20,
  },
  txtsaldoTitle: {marginLeft: 10, color: colors.white},
  name: {
    marginLeft: 10,
    fontWeight: 'bold',
  },
  btnLanjut: {
    elevation: 5,
    height: 50,
    backgroundColor: colors.primary,
    alignItems: 'center',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  navTitle: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  txtLanjut: {color: colors.white},
  iconback: {
    height: 30,
    width: 30,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
    position: 'absolute',
    top: 0,
    left: 10,
  },
  infoContainer: {
    padding: 10,
    elevation: 5,
    backgroundColor: colors.white,
    marginTop: 10,
  },
  itemPrice: {
    position: 'absolute',
    right: 10,
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 40,
  },
  txtSaldo: {color: colors.white},
  navBar: {
    paddingVertical: 10,
    backgroundColor: 'deepskyblue',
  },
  line: {flex: 1, height: 1},
  headerContainer: {
    height: height / 3,
    backgroundColor: 'grey',
    width: width,
  },
  iconUser: {
    height: 30,
    width: 30,
    backgroundColor: colors.light,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
  },
  textInput: {
    paddingHorizontal: 10,
    height: 40,
    borderColor: colors.white,
    borderWidth: 1,
    flex: 1,
    marginHorizontal: 40,
    borderRadius: 10,
    color: colors.white,
  },
  headerContent: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  card: {
    padding: 15,
    borderRadius: 10,
    backgroundColor: colors.white,
    flex: 1,
    elevation: 5,
    margin: 10,
  },
  btnContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    borderRadius: 10,
    backgroundColor: colors.white,
    flex: 1,
    marginHorizontal: 10,
  },
  btnMoney: {
    backgroundColor: colors.primary,
    padding: 10,
    borderRadius: 5,
  },

  carditem: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  name: {
    fontSize: 14,
  },
  desc: {
    marginVertical: 10,
    fontSize: 14,
  },
  money: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  btnImage: {justifyContent: 'center', alignItems: 'center'},
  imgCard: {height: height / 3, width: '100%'},
  imgAvatar: {height: 30, width: 30, borderRadius: 30},
  cardcontent: {paddingLeft: 10, width: '50%'},
  imageComponentStyle: {
    borderRadius: 15,
    width: '95%',
    marginVertical: 5,
    marginHorizontal: 20,
    backgroundColor: colors.white,
  },
  iconSaldo: {flexDirection: 'row', alignItems: 'center'},
  image: {height: 30, width: 30},
  btnImageSize: {height: 40, width: 40},
  centeredView: {
    flex: 1,
    justifyContent: 'flex-end',
    marginTop: 0,
    backgroundColor: colors.transparent,
  },
  modalView: {
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    padding: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },

  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
  },
  btnAmount: {
    padding: 10,
    elevation: 5,
    borderRadius: 10,
    backgroundColor: colors.white,
    width: '45%',
    marginBottom: 10,
    marginHorizontal: 5,
    marginTop: 10,
  },
  loadingContainer: {marginLeft: 10},
});

export default DetailDonasi;
