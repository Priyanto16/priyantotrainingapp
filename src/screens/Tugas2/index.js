import React from 'react';
import {
  Image,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
const index = () => {
  const price = '120.000.000';
  const name = 'Priyanto SL';
  const imageUri =
    'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/8884d94c-ad7a-4cbd-b83b-6b1f626aa2eb/ddwupg2-81168f56-2d38-4b0b-aaa3-8b6d5d8f988b.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvODg4NGQ5NGMtYWQ3YS00Y2JkLWI4M2ItNmIxZjYyNmFhMmViXC9kZHd1cGcyLTgxMTY4ZjU2LTJkMzgtNGIwYi1hYWEzLThiNmQ1ZDhmOTg4Yi5qcGcifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.kpBavVoYVME4FZOLnyH-mPDG70E_yNLIEKme2lEn_Yg';

  const Navbar = (props) => (
    <View style={styles.navBar}>
      <Text style={styles.navTitle}>{props.title}</Text>
    </View>
  );
  const Header = () => (
    <View style={styles.item}>
      <Image
        resizeMethod="resize"
        resizeMode="cover"
        source={{uri: imageUri}}
        style={styles.avatar}
      />
      <Text style={styles.name}>{name}</Text>
    </View>
  );

  const Item = (props) => (
    <TouchableOpacity style={styles.item}>
      <Icon name={props.icon} size={22} color={'grey'} />
      <Text style={styles.itemLabel}>{props.name}</Text>
      <Text style={styles.itemPrice}>{props.price}</Text>
    </TouchableOpacity>
  );

  const Divider = (props) => <View style={{height: props.height}} />;
  const Line = () => <View style={styles.line} />;

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="deepskyblue" />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}>
        <View style={styles.content}>
          <Navbar title="Account" />
          <Header />
          <Item name="Saldo" icon="credit-card" price={price} />
          <Divider height={5} />
          <Item name="Pengaturan" icon="settings" />
          <Line />
          <Item name="Bantuan" icon="help-circle" />
          <Line />
          <Item name="Syarat & Ketentuan" icon="clipboard" />
          <Divider height={5} />
          <Item name="Keluar" icon="log-out" />
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'whitesmoke',
  },
  content: {
    padding: 0,
  },
  item: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  itemLabel: {
    marginLeft: 20,
  },
  name: {
    marginLeft: 10,
    fontWeight: 'bold',
  },
  navTitle: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  itemPrice: {
    position: 'absolute',
    right: 10,
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 20,
  },
  navBar: {
    paddingVertical: 10,
    backgroundColor: 'deepskyblue',
  },
  line: {flex: 1, height: 1},
});

export default index;
