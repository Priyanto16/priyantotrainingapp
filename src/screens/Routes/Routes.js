import AsyncStorage from '@react-native-async-storage/async-storage';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React, {useContext, useEffect, useState} from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {RootContext} from '../../../App';
import colors from '../../style/colors';
import Account from '../Account';
import Charts from '../Charts';
import Chat from '../Chat';
import CreateDonation from '../CreateDonation';
import Donasi from '../Donasi';
import DetailDonasi from '../Donasi/DetailDonasi';
import Home from '../Home';
import Intro from '../Intro';
import Login from '../Login';
import Maps from '../Maps';
import Payments from '../Payments';
import Profile from '../Profile';
import Register from '../Register';
import InputOTP from '../Register/InputOTP';
import SetupPassword from '../Register/SetupPassword';
import Transaksi from '../Transaksi';
import SplashScreens from '../Splashscreen';

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();
const iconSize = 26;
const MainTabs = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor={colors.primary}
      inactiveColor={colors.dark}
      barStyle={{backgroundColor: colors.white}}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="home" color={color} size={iconSize} />
          ),
        }}
      />
      <Tab.Screen
        name="Chat"
        component={Chat}
        options={{
          tabBarLabel: 'Chat',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="chat" color={color} size={iconSize} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name="account"
              color={color}
              size={iconSize}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const options = {
  headerTitleAlign: 'center',
  headerShown: true,
  headerStyle: {
    backgroundColor: colors.primary,
  },
  headerTintColor: colors.white,
};

const SigninNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="MainTab"
      component={MainTabs}
      options={{headerShown: false}}
    />

    <Stack.Screen name="Profile" component={Profile} options={options} />
    <Stack.Screen name="Account" component={Account} options={options} />
    <Stack.Screen name="Donasi" component={Donasi} options={options} />
    <Stack.Screen name="Payments" component={Payments} options={options} />
    <Stack.Screen name="Maps" component={Maps} options={options} />
    <Stack.Screen name="Charts" component={Charts} options={options} />
    <Stack.Screen name="Transaksi" component={Transaksi} options={options} />
    <Stack.Screen
      name="CreateDonation"
      component={CreateDonation}
      options={options}
    />
    <Stack.Screen
      name="DetailDonasi"
      component={DetailDonasi}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="SignoutNavigator"
      component={SignoutNavigator}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

const SignoutNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Intro"
      component={Intro}
      options={{headerShown: false}}
    />
    <Stack.Screen name="Login" component={Login} options={options} />
    <Stack.Screen
      title="Test"
      name="Register"
      component={Register}
      options={options}
    />
    <Stack.Screen
      name="SetupPassword"
      component={SetupPassword}
      options={options}
    />
    <Stack.Screen name="InputOTP" component={InputOTP} options={options} />
    <Stack.Screen
      name="SigninNavigator"
      component={SigninNavigator}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

const AppNavigation = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [isLogin, setIsLogin] = useState(false);
  const {onsetLoginType} = useContext(RootContext);

  useEffect(() => {
    async function getLoginStatus() {
      try {
        let isLogin = await AsyncStorage.getItem('isLogin');
        setIsLogin(isLogin == 'true' ? true : false);
      } catch (error) {
        console.log('Something went wrong', error);
      }
    }

    async function getLoginType() {
      try {
        let loginType = await AsyncStorage.getItem('loginType');
        onsetLoginType(loginType);
      } catch (error) {
        console.log('Something went wrong', error);
      }
    }
    getLoginStatus();
    getLoginType();

    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);
  }, []);

  if (isLoading) {
    return <SplashScreens />;
  }

  return (
    <NavigationContainer>
      {isLogin ? <SigninNavigator /> : <SignoutNavigator />}
    </NavigationContainer>
  );
};

export default AppNavigation;
