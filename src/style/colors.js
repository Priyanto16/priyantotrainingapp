'use strict';

export default {
  primary: 'deepskyblue',
  secondary: '#f194ff',
  white: '#FFF',
  lighter: '#F3F3F3',
  light: '#DAE1E7',
  dark: '#444',
  black: '#000',
  red: '#ff0000',
  dodgerblue: `#1e90ff`,
  whitesmoke: `whitesmoke`,
  deeppink: '#ff1493',
  palegoldenrod: `#eee8aa`,
  orange: `#ffa500`,
  transparent: 'rgba(0, 0, 0, 0.5)',
};
