import React, {useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  SafeAreaView,
  processColor,
} from 'react-native';
import colors from '../../style/colors';
import {BarChart} from 'react-native-charts-wrapper';

const Index = () => {
  const legend = {
    enabled: false,
  };

  const data = {
    dataSets: [
      {
        values: [
          {y: 100},
          {y: 60},
          {y: 90},
          {y: 45},
          {y: 67},
          {y: 32},
          {y: 150},
          {y: 70},
          {y: 40},
          {y: 89},
        ],
        label: 'Data',
        config: {
          color: processColor(colors.deeppink),
          stackLabels: [],
          drawFilled: false,
          drawValues: false,
        },
      },
    ],
  };

  const xAxis = {
    valueFormatter: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
    ],
    granularityEnabled: true,
    granularity: 1,
    position: 'BOTTOM',
  };

  const yAxis = {
    left: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      drawGridLines: false,
    },
    right: {
      enabled: false,
    },
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
      <View style={styles.container}>
        <BarChart
          style={styles.chart}
          data={data}
          xAxis={xAxis}
          yAxis={yAxis}
          visibleRange={{x: {min: 10, max: 10}}}
          animation={{durationX: 2000}}
          drawBarShadow={false}
          drawValueAboveBar={true}
          drawHighlightArrow={true}
          chartDescription={{text: ''}}
          legend={legend}
          marker={{
            enabled: true,
            markerColor: processColor(colors.primary),
            textColor: processColor(colors.white),
            textSize: 14,
          }}
          gridBackgroundColor={processColor(colors.white)}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 10,
  },
  chart: {
    flex: 1,
  },
  scrollView: {
    backgroundColor: 'whitesmoke',
  },
  content: {
    padding: 0,
  },
});

export default Index;
