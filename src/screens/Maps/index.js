import React, {useEffect} from 'react';
import MapboxGL from '@react-native-mapbox-gl/maps';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import colors from '../../style/colors';
import Icon from 'react-native-vector-icons/Feather';

MapboxGL.setAccessToken(
  'pk.eyJ1IjoicHJpeWFudG8xNiIsImEiOiJja2l6ZXV3c28wNm5kMnJwaG85a2VucmhwIn0.krIpYtoZCGS852aFS_TVVg',
);
const Index = () => {
  const coordinates = [
    [107.58011, -6.890066],
    [106.819449, -6.218465],
    [110.365231, -7.795766],
  ];
  useEffect(() => {
    const getLocation = async () => {
      try {
        const permission = await MapboxGL.requestAndroidLocationPermissions();
      } catch (error) {
        console.log(error);
      }
    };
    getLocation();
  }, []);

  const Item = (props) => (
    <TouchableOpacity style={styles.item}>
      <Icon name={props.icon} size={22} color={'grey'} />
      <Text style={styles.itemLabel}>{props.name}</Text>
      <Text style={styles.itemPrice}>{props.price}</Text>
    </TouchableOpacity>
  );

  const Divider = (props) => <View style={{height: props.height}} />;
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
      <MapboxGL.MapView style={styles.container}>
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera fol />
        {coordinates.map((item, index) => (
          <MapboxGL.PointAnnotation id={index.toString()} coordinate={item}>
            <MapboxGL.Callout
              title={`latitude: ${item[0]}, longngitude: ${item[1]}`}
            />
          </MapboxGL.PointAnnotation>
        ))}

        <MapboxGL.Camera
          animationMode={'flyTo'}
          animationDuration={0}
          zoomLevel={6}
          centerCoordinate={coordinates[1]}
        />
      </MapboxGL.MapView>
      <View>
        <Item name="Bandung, Jakarta, Yogyakarta" icon="home" />
        <Divider height={5} />
        <Item name="customer@crowdfunding.com" icon="mail" />
        <Divider height={5} />
        <Item name="(021) 123123" icon="phone" />
        <Divider height={5} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    backgroundColor: 'whitesmoke',
  },
  content: {
    padding: 0,
  },
  item: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  itemLabel: {
    marginLeft: 20,
  },
  name: {
    marginLeft: 10,
    fontWeight: 'bold',
  },
  navTitle: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  itemPrice: {
    position: 'absolute',
    right: 10,
  },
  avatar: {
    height: 60,
    width: 60,
    borderRadius: 60,
  },
  navBar: {
    paddingVertical: 10,
    backgroundColor: 'deepskyblue',
  },
  line: {flex: 1, height: 1},
});

export default Index;
