import React, {useState, useCallback, useEffect, useContext} from 'react';
import {GiftedChat} from 'react-native-gifted-chat';
import database from '@react-native-firebase/database';
import {RootContext} from '../../../App';

const index = () => {
  const {state, setUserData} = useContext(RootContext);
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    onRef();
    return () => {
      const db = database().ref('messages');
      if (db) db.off();
    };
  }, []);

  const onRef = () => {
    database()
      .ref('messages')
      .limitToLast(20)
      .on('child_added', (snapshot) => {
        const value = snapshot.val();
        setMessages((previousMessages) =>
          GiftedChat.append(previousMessages, value),
        );
      });
  };

  const onSend = useCallback((messages = []) => {
    for (let i = 0; i < messages.length; i++) {
      database()
        .ref('messages')
        .push({
          _id: messages[i]._id,
          text: messages[i].text,
          createdAt: new Date(),
          user: {
            _id: 2,
            name: state.name,
            avatar: state.image,
          },
        });
    }
  }, []);

  return (
    <GiftedChat
      messages={messages}
      onSend={(messages) => onSend(messages)}
      user={{
        _id: 1,
      }}
    />
  );
};

export default index;
