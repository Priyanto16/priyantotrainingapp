import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  TextInput,
  Text,
  ToastAndroid,
} from 'react-native';
import Axios from 'axios';
import Separator from '../../components/Separator';
import colors from '../../style/colors';
import api from '../../api';
import Icon from 'react-native-vector-icons/Feather';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Button from '../../components/Button';

const SetupPassword = ({route, navigation}) => {
  const [password_confirmation, setPasswordConfirm] = useState('');
  const [showpassword, setshowPassword] = useState(false);
  const [password, setPassword] = useState('');
  const {name, email} = route.params;

  useEffect(() => {
    navigation.setOptions({title: 'Ubah Kata Sandi'});
  }, []);

  const onSave = () => {
    if (password.length < 6 || password_confirmation.length < 6)
      return ToastAndroid.show(
        'Password minimal 6 karakter',
        ToastAndroid.SHORT,
      );

    if (password.length !== password_confirmation.length)
      return ToastAndroid.show('Password tidak sama', ToastAndroid.SHORT);

    let data = {
      email,
      password,
      password_confirmation,
    };
    Axios.post(`${api}/auth/update-password`, data, {
      timeout: 20000,
    })
      .then((res) => {
        console.log('update-password -> res', res);
        let data = res.data;
        if (data.response_code == '00') {
          navigation.navigate('Login');
          ToastAndroid.show(data.response_message, ToastAndroid.SHORT);
        } else {
          ToastAndroid.show('update password gagal!', ToastAndroid.SHORT);
        }
      })
      .catch((err) => {
        ToastAndroid.show('update password gagal!', ToastAndroid.SHORT);
        console.log(err);
      });
  };

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="deepskyblue" />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}>
        <View style={styles.content}>
          <Text style={styles.txtgreting}>
            Tentukan kata sandi baru untuk keamanan akun kamu!
          </Text>
          <Separator />
          <Text>Password</Text>
          <View style={styles.inputContainer}>
            <TextInput
              secureTextEntry={showpassword}
              placeholder="password"
              style={styles.txtInput}
              onChangeText={(text) => setPassword(text)}
              value={password}
            />
            <TouchableOpacity onPress={() => setshowPassword(!showpassword)}>
              <Icon
                name={showpassword ? 'eye' : 'eye-off'}
                size={22}
                color={colors.black}
              />
            </TouchableOpacity>
          </View>
          <Separator />
          <Text>Konfirmasi password</Text>
          <View style={styles.inputContainer}>
            <TextInput
              secureTextEntry={showpassword}
              placeholder="password"
              style={styles.txtInput}
              onChangeText={(text) => setPasswordConfirm(text)}
              value={password_confirmation}
            />
            <TouchableOpacity onPress={() => setshowPassword(!showpassword)}>
              <Icon
                name={showpassword ? 'eye' : 'eye-off'}
                size={22}
                color={colors.black}
              />
            </TouchableOpacity>
          </View>
          <Separator />
          <Button
            title="SIMPAN"
            color={colors.primary}
            onPress={() => onSave()}
          />
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'whitesmoke',
  },
  content: {
    padding: 20,
  },
  item: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  inputContainer: {flexDirection: 'row', alignItems: 'center', flex: 1},
  itemLabel: {
    marginLeft: 20,
  },
  name: {
    marginLeft: 10,
    fontWeight: 'bold',
  },
  navTitle: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  itemPrice: {
    position: 'absolute',
    right: 10,
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 20,
  },
  navBar: {
    paddingVertical: 10,
    backgroundColor: 'deepskyblue',
  },
  line: {flex: 1, height: 1},
  txtInput: {height: 40, borderBottomWidth: StyleSheet.hairlineWidth, flex: 1},
  txtOr: {alignSelf: 'center', color: colors.dark},
  txtOrContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    justifyContent: 'center',
  },
  txtLogin: {
    color: colors.primary,
    fontWeight: 'bold',
    marginLeft: 10,
  },
  txtgreting: {
    color: colors.black,
    fontWeight: 'bold',
  },
});

export default SetupPassword;
