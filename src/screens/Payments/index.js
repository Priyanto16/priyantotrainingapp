import React from 'react';
import {StyleSheet, StatusBar, SafeAreaView} from 'react-native';
import {WebView} from 'react-native-webview';
import colors from '../../style/colors';

const Index = ({route}) => {
  const data = route.params.data;
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
      <WebView source={{uri: data.midtrans.redirect_url}} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default Index;
