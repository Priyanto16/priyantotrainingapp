import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
const Navbar = (props) => {
  return (
    <View style={styles.navBar}>
      <Text style={styles.navTitle}>{props.title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  navTitle: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  navBar: {
    paddingVertical: 10,
    backgroundColor: 'deepskyblue',
  },
});

export default Navbar;
