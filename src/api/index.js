const api = 'https://crowdfunding.sanberdev.com/api';
export const base_url = 'https://crowdfunding.sanberdev.com/';
export const host = 'https://crowdfunding.sanberdev.com';
export default api;
