import React from 'react';
import {View, Text, Image, StatusBar, SafeAreaView, Alert} from 'react-native';
import styles from './style';
import colors from '../../style/colors';
import Separator from '../../components/Separator';
import Button from '../../components/Button';
import AppIntroSlider from 'react-native-app-intro-slider';

const data = [
  {
    id: 1,
    image: require('../../assets/images/intro_image2.jpg'),
    description:
      'Ayo kita mulai perubahan pada dunia dengan hal kecil dimulai dari dirimu!.',
  },
  {
    id: 2,
    image: require('../../assets/images/intro_image1.jpg'),
    description:
      'Berapapun donasi yang kamu berikan sangat berarti bagi orang lain.',
  },
  {
    id: 3,
    image: require('../../assets/images/intro_image3.jpg'),
    description: 'Terus semangat sebarkan kebaikan.',
  },
];

const Intro = ({navigation}) => {
  const renderItem = ({item}) => {
    return (
      <View style={styles.listContainer}>
        <View style={styles.listContent}>
          <Image
            source={item.image}
            style={styles.imgList}
            resizeMethod="auto"
            resizeMode="contain"
          />
          <Text style={styles.textList}>{item.description}</Text>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        backgroundColor={colors.white}
        barStyle="dark-content"
        animated
      />
      <View style={styles.textLogoContainer}>
        <Text style={styles.textLogo}>CrowdFunding</Text>
      </View>
      <View style={styles.slider}>
        <AppIntroSlider
          data={data}
          renderItem={renderItem}
          activeDotStyle={styles.activeDotStyle}
          keyExtractor={(item) => item.id.toString()}
        />
      </View>

      <View style={styles.btnContainer}>
        <Button
          title="MASUK"
          color={colors.primary}
          onPress={() => navigation.navigate('Login')}
        />
        <Separator />
        <Button
          type="secondary"
          title="DAFTAR"
          color={colors.white}
          onPress={() => navigation.navigate('Register')}
        />
      </View>
    </SafeAreaView>
  );
};

export default Intro;
