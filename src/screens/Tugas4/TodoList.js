import React, {useState, useContext} from 'react';
import {
  Image,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  TextInput,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import Navbar from './../../components/Navbar';
import {RootContext} from '../Tugas4';
const TodoList = () => {
  const {input, todos, handleChangeInput, addTodo, removeTodo} = useContext(
    RootContext,
  );

  const renderItem = ({item}) => (
    <View style={styles.itemContainer}>
      <View style={{flex: 1}}>
        <Text style={styles.txtDate}>{item.date}</Text>
        <Text>{item.label}</Text>
      </View>
      <View>
        <TouchableOpacity onPress={() => removeTodo(item.id)}>
          <Icon name={'trash'} size={22} color={'#000'} />
        </TouchableOpacity>
      </View>
    </View>
  );

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="deepskyblue" />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}>
        <View style={styles.content}>
          <Navbar title="Masukan Todolist" />
          <View style={styles.inputContainer}>
            <TextInput
              placeholder={'masukan disini'}
              onChangeText={(text) => handleChangeInput(text)}
              value={input}
              style={styles.txtInput}
            />
            <TouchableOpacity style={styles.btnAdd} onPress={() => addTodo()}>
              <Icon name={'plus'} size={22} color={'white'} />
            </TouchableOpacity>
          </View>
          <View>
            <FlatList
              data={todos}
              renderItem={renderItem}
              keyExtractor={(item) => item.id.toString()}
              style={styles.flatList}
            />
          </View>
          {todos.length || <Text style={styles.txtEmpty}>Belum ada todo.</Text>}
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'whitesmoke',
  },
  content: {
    padding: 0,
  },
  btnAdd: {
    borderRadius: 5,
    backgroundColor: 'deepskyblue',
    height: 50,
    width: 50,
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  flatList: {marginTop: 5},
  item: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  itemContainer: {
    padding: 10,
    backgroundColor: 'white',
    marginVertical: 5,
    borderRadius: 5,
    elevation: 3,
    borderBottomWidth: 0.2,
    borderTopWidth: 0.2,
    borderColor: '#000',
    alignItems: 'center',
    flexDirection: 'row',
    marginHorizontal: 10,
  },
  txtInput: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'lightgrey',
    flex: 1,
    height: 50,
    paddingHorizontal: 10,
  },
  inputContainer: {
    elevation: 3,
    flex: 1,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  txtDate: {fontSize: 10},
  txtEmpty: {
    marginVertical: 10,
    alignSelf: 'center',
    color: 'lightslategrey',
  },
  navTitle: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  navBar: {
    paddingVertical: 10,
    backgroundColor: 'deepskyblue',
  },
});

export default TodoList;
