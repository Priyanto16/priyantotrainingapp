import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  TextInput,
  Text,
  ToastAndroid,
} from 'react-native';
import Axios from 'axios';
import Separator from '../../components/Separator';
import colors from '../../style/colors';
import api from '../../api';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Button from '../../components/Button';
import InputError from '../../components/InputError';

const index = ({navigation}) => {
  const [email, setUsername] = useState('');
  const [name, setName] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [validation, setValidation] = useState({
    email: true,
    name: true,
  });
  useEffect(() => {
    navigation.setOptions({title: 'Daftar'});
  }, []);

  const onRegisterPress = () => {
    if (name.length > 0 && email.length > 0) {
      setLoading(true);
      let data = {
        email,
        name,
      };
      Axios.post(`${api}/auth/register`, data, {
        timeout: 20000,
      })
        .then((res) => {
          console.log('register -> res', res);
          let data = res.data;
          if (data.response_code == '00') {
            navigation.navigate('InputOTP', {name: name, email: email});
            ToastAndroid.show(data.response_message, ToastAndroid.SHORT);
            setLoading(false);
          } else {
            ToastAndroid.show('Register gagal!', ToastAndroid.SHORT);
            setLoading(false);
          }
        })
        .catch((err) => {
          ToastAndroid.show('Register gagal!', ToastAndroid.SHORT);
          console.log(err);
          setLoading(false);
        });
    } else {
      setValidation({
        name: name ? true : false,
        email: email ? true : false,
      });
    }
  };

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="deepskyblue" />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}>
        <View style={styles.content}>
          <Text>Email</Text>
          <TextInput
            placeholder="Email"
            style={styles.txtInput}
            onChangeText={(text) => {
              setUsername(text);
              setValidation({
                ...validation,
                email: text.length > 0 ? true : false,
              });
            }}
            value={email}
          />
          {!validation.email && <InputError text={'Masukan Email'} />}

          <Separator />
          <Text>Nama Lengkap</Text>
          <TextInput
            placeholder="name"
            style={styles.txtInput}
            onChangeText={(text) => {
              setName(text);
              setValidation({
                ...validation,
                name: text.length > 0 ? true : false,
              });
            }}
            value={name}
          />
          {!validation.name && <InputError text={'Masukan Nama'} />}

          <Separator />
          <Button
            isLoading={isLoading}
            title="DAFTAR"
            color={colors.primary}
            onPress={() => onRegisterPress()}
          />
          <View style={styles.txtOrContainer}>
            <Text style={styles.txtOr}>Sudah punya akun?</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
              <Text style={styles.txtLogin}>Masuk</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'whitesmoke',
  },
  content: {
    padding: 20,
  },
  item: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  itemLabel: {
    marginLeft: 20,
  },
  name: {
    marginLeft: 10,
    fontWeight: 'bold',
  },
  navTitle: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  itemPrice: {
    position: 'absolute',
    right: 10,
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 20,
  },
  navBar: {
    paddingVertical: 10,
    backgroundColor: 'deepskyblue',
  },
  line: {flex: 1, height: 1},
  txtInput: {height: 40, borderBottomWidth: StyleSheet.hairlineWidth},
  txtOr: {alignSelf: 'center', color: colors.dark},
  txtOrContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    justifyContent: 'center',
  },
  txtLogin: {
    color: colors.primary,
    fontWeight: 'bold',
    marginLeft: 10,
  },
});

export default index;
