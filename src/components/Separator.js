import React from 'react';
import {StyleSheet, View} from 'react-native';

const Separator = () => <View style={styles.separator} />;
const styles = StyleSheet.create({
  separator: {
    marginVertical: 4,
    marginHorizontal: 4,
  },
});

export default Separator;
