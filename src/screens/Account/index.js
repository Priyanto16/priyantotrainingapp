import React, {useEffect, useState, useContext, useRef} from 'react';
import {
  Image,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  TouchableOpacity,
  Modal,
  Dimensions,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/Feather';
import api from '../../api';
import {base_url} from '../../api';
import colors from '../../style/colors';
import InputItem from '../../components/InputItem';
import {RootContext} from '../../../App';
import {SafeAreaView} from 'react-native-safe-area-context';
import Button from '../../components/Button';
import InputError from '../../components/InputError';
const {height, width} = Dimensions.get('window');

const index = ({navigation}) => {
  const {state, setUserData} = useContext(RootContext);
  let input = useRef(null);
  let camera = useRef(null);
  const [isVisible, setIsVisible] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);
  const [name, setName] = useState(state.name);
  const [token, setToken] = useState('');
  const [validation, setValidation] = useState({
    name: true,
  });
  useEffect(() => {
    getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token', token);
        setToken(token);
        console.log('profile -> token', token);
      } catch (err) {
        console.log(err);
      }
    };

    getToken();
    console.log('name', state.name);
  }, []);

  const onChangeText = (text) => {
    console.log(text);
    setName(text);
    setValidation({name: text.length > 0 ? true : false});
  };

  const togleCamera = () => {
    setType(type == 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    if (camera) {
      const options = {quality: 0.5, base64: true};
      const data = await camera.current.takePictureAsync(options);
      console.log(data.uri);
      setPhoto(data.uri);
      setIsVisible(false);
    }
  };

  const onSave = () => {
    if (name.length > 0) {
      setLoading(true);
      const formData = new FormData();
      formData.append('name', name);
      if (photo) {
        formData.append('photo', {
          uri: photo,
          name: 'photo.jpg',
          type: 'image/jpg',
        });
      }
      Axios.post(`${api}/profile/update-profile`, formData, {
        timeout: 20000,
        headers: {
          Authorization: 'Bearer ' + token,
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
        },
      })
        .then((res) => {
          if (res.data.response_code == '00') {
            setUserData(
              res.data.data.profile.name,
              res.data.data.profile.email,
              `${base_url + res.data.data.profile.photo}`,
            );
            setLoading(false);
            navigation.navigate('Profile');
          }
          console.log('update-profile -> res', res);
        })
        .catch((err) => {
          setLoading(false);
          console.log(err);
        });
    } else {
      setValidation({
        name: name ? true : false,
      });
    }
  };

  const Avatar = () => (
    <View style={styles.avatarContainer}>
      <Image
        resizeMethod="resize"
        resizeMode="cover"
        source={
          photo
            ? {uri: photo}
            : state.image
            ? {
                uri: state.image + '?' + new Date(),
                cache: 'reload',
                headers: {Pragma: 'no-cache'},
              }
            : require('../../assets/images/no_image.png')
        }
        style={styles.avatar}
      />
      <TouchableOpacity
        style={styles.iconContainer}
        onPress={() => setIsVisible(true)}>
        <Icon name={'camera'} size={22} color={colors.lighter} />
      </TouchableOpacity>
    </View>
  );

  const Divider = (props) => <View style={{height: props.height}} />;
  const Line = () => <View style={styles.line} />;

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1}}
            type={type}
            ref={camera}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}>
            <View style={styles.shapeContainer}>
              <View style={styles.shapeTop} />
              <View style={styles.camerBtnContainer} />
            </View>
            <View style={styles.shapeBotom}>
              <TouchableOpacity
                style={styles.cameraIcon}
                onPress={() => takePicture()}>
                <Icon name={'camera'} size={33} color={colors.dark} />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.switcCamera}
                onPress={() => togleCamera()}>
                <Icon name={'refresh-cw'} size={22} color={colors.dark} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="deepskyblue" />
      <SafeAreaView style={styles.container}>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.content}>
            <Avatar />
            <Line />
            <InputItem
              name="name"
              icon={'user'}
              value={name}
              onChangeText={onChangeText}
              placeholder={'Nama lengkap'}
            />
            {!validation.name && <InputError text={'Masukan Nama'} />}
            <InputItem
              name="email"
              icon={'mail'}
              value={state.email}
              onChangeText={() => onChangeText}
              placeholder={'Email'}
            />
            <Divider height={5} />
            <Button
              isLoading={isLoading}
              title="SAVE"
              color={colors.primary}
              onPress={() => onSave()}
            />
          </View>
        </ScrollView>
        {renderCamera()}
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
    backgroundColor: colors.white,
  },
  content: {
    paddingHorizontal: 20,
  },
  item: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  avatarContainer: {
    padding: 10,
    alignItems: 'center',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  itemLabel: {
    marginLeft: 20,
  },
  name: {
    marginLeft: 10,
    fontWeight: 'bold',
  },
  navTitle: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  itemPrice: {
    position: 'absolute',
    right: 10,
  },
  avatar: {
    height: 100,
    width: 100,
    borderRadius: 50,
    backgroundColor: colors.lighter,
  },
  navBar: {
    paddingVertical: 10,
    backgroundColor: 'deepskyblue',
  },
  line: {flex: 1, height: 1},
  iconContainer: {
    height: 33,
    width: 33,
    backgroundColor: 'grey',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 33,
    position: 'absolute',
    right: 10,
    bottom: 10,
  },
  switcCamera: {
    height: 60,
    width: 60,
    borderRadius: 50,
    backgroundColor: colors.white,
    position: 'absolute',
    right: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  camerBtnContainer: {
    borderColor: colors.palegoldenrod,
    borderWidth: 2,
    height: 150,
    width: width - 100,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  cameraIcon: {
    height: 80,
    width: 80,
    borderRadius: 50,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  shapeContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  shapeTop: {
    borderWidth: 2,
    borderColor: colors.palegoldenrod,
    height: 150,
    width: width / 2.5,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 20,
  },
  shapeBotom: {
    width: width,
    position: 'absolute',
    bottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default index;
