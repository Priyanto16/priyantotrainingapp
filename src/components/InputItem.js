import React, {useEffect, useState} from 'react';
import {
  Image,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  TouchableOpacity,
  TextInput,
  Text,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

const InputItem = (props) => {
  const [name, setName] = useState(props.value);
  const [editable, setEditable] = useState(false);
  return (
    <View style={styles.container}>
      {props.icon ? (
        <View style={{marginRight: 10}}>
          <Icon name={props.icon} size={22} color={'grey'} />
        </View>
      ) : null}
      <TextInput
        placeholder={props.placeholder}
        style={styles.txtInput}
        onChangeText={(text) => {
          setName(text);
          props.onChangeText(text);
        }}
        value={name}
        editable={props.name == 'email' ? false : editable}
      />
      {props.name == 'name' && (
        <TouchableOpacity onPress={() => setEditable(editable ? false : true)}>
          <Icon name={'edit'} size={22} color={'grey'} />
        </TouchableOpacity>
      )}
    </View>
  );
};
export default InputItem;

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  txtInput: {
    height: 40,
    borderBottomWidth: StyleSheet.hairlineWidth,
    flex: 1,
  },
});
