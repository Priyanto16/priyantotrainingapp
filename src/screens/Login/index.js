import React, {useState, useEffect, useContext} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  TextInput,
  Text,
  ToastAndroid,
  ActivityIndicator,
} from 'react-native';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Separator from '../../components/Separator';
import colors from '../../style/colors';
import api from '../../api';
import auth from '@react-native-firebase/auth';
import {CommonActions} from '@react-navigation/native';
import {
  GoogleSignin,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id';
import {RootContext} from '../../../App';
import Button from '../../components/Button';
import InputError from '../../components/InputError';

const index = ({navigation}) => {
  const [email, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [validation, setValidation] = useState({
    email: true,
    password: true,
  });
  const {onsetLoginType} = useContext(RootContext);
  const [isLoading, setLoading] = useState(false);
  const config = {
    title: 'Authentication Required',
    imageColor: colors.primary,
    imageErrorColor: colors.red,
    sensorDescription: 'Touch sensor',
    sensorErrorDescription: 'Failed',
    cancelText: 'Cancel',
    fallbackLabel: 'Show Passcode',
    unifiedErrors: false,
    passcodeFallback: false,
  };

  const saveToken = async (token) => {
    try {
      await AsyncStorage.setItem('token', token);
    } catch (err) {
      console.log(err);
    }
  };

  const configureGoogleSignin = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '932606465253-i5i8dt885vdn5s2jhlk409t7o44pf3c4.apps.googleusercontent.com',
    });
  };

  useEffect(() => {
    configureGoogleSignin();
  }, []);

  const signinWithGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const {idToken} = await GoogleSignin.signIn();
      if (idToken) {
        const credential = auth.GoogleAuthProvider.credential(idToken);
        console.log(idToken);
        auth().signInWithCredential(credential);
        onsetLoginType('google');
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{name: 'SigninNavigator'}],
          }),
        );
        ToastAndroid.show('Login berhasil', ToastAndroid.SHORT);
        await AsyncStorage.setItem('isLogin', 'true');
      }
      console.log('idToken', idToken);
    } catch (error) {
      console.log('error', error);
    }
  };

  const signinWithFingerprint = async () => {
    TouchID.authenticate('', config)
      .then(async (success) => {
        console.log(success);
        ToastAndroid.show('Authentification Success', ToastAndroid.SHORT);
        await AsyncStorage.setItem('isLogin', 'true');
        onsetLoginType('fingerprint');
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{name: 'SigninNavigator'}],
          }),
        );
      })
      .catch((error) => {
        console.log(error);
        ToastAndroid.show('Authentification Failed', ToastAndroid.SHORT);
      });
  };

  const onLoginPress = () => {
    if (email.length > 0 && password.length > 0) {
      setLoading(true);
      let data = {
        email,
        password,
      };
      Axios.post(`${api}/auth/login`, data, {
        timeout: 20000,
      })
        .then(async (res) => {
          console.log('login -> res', res);
          let data = res.data;
          if (data.response_code == '00') {
            saveToken(data.data.token);
            navigation.dispatch(
              CommonActions.reset({
                index: 0,
                routes: [{name: 'SigninNavigator'}],
              }),
            );
            onsetLoginType('default');
            setLoading(false);
            await AsyncStorage.setItem('isLogin', 'true');
            ToastAndroid.show(data.response_message, ToastAndroid.SHORT);
          } else {
            ToastAndroid.show('Login gagal!', ToastAndroid.SHORT);
          }
        })
        .catch((err) => {
          ToastAndroid.show('Login gagal!', ToastAndroid.SHORT);
          console.log(err);
          setLoading(false);
        });
    } else {
      setValidation({
        password: password ? true : false,
        email: email ? true : false,
      });
    }
  };

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="deepskyblue" />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}>
        <View style={styles.content}>
          <Text>Email</Text>
          <TextInput
            placeholder="Email"
            style={styles.txtInput}
            onChangeText={(text) => {
              setValidation({
                ...validation,
                email: text.length > 0 ? true : false,
              });

              setUsername(text);
            }}
            value={email}
          />
          {!validation.email && <InputError text={'Masukan Email'} />}

          <Separator />
          <Text>Password</Text>
          <TextInput
            secureTextEntry
            placeholder="Password"
            style={styles.txtInput}
            onChangeText={(text) => {
              setValidation({
                ...validation,
                password: text.length > 0 ? true : false,
              });
              setPassword(text);
            }}
            value={password}
          />

          {!validation.password && <InputError text={'Masukan Password'} />}
          <Separator />
          <Button
            isLoading={isLoading}
            title="MASUK"
            color={colors.primary}
            onPress={() => onLoginPress()}
          />

          <View style={styles.txtOrContainer}>
            <Text style={styles.txtOr}>Masuk dengan lebih cepat</Text>
          </View>
          <View style={styles.btnContainer}>
            <Button
              type={'secondary'}
              withIcon={true}
              title="GOOGLE"
              color={colors.white}
              onPress={() => signinWithGoogle()}
            />
            <Separator />
            <Button
              type={'secondary'}
              withIcon={true}
              title="FINGERPRINT"
              color={colors.white}
              onPress={() => signinWithFingerprint()}
            />
          </View>
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'whitesmoke',
  },
  content: {
    padding: 20,
  },
  item: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  itemLabel: {
    marginLeft: 20,
  },
  name: {
    marginLeft: 10,
    fontWeight: 'bold',
  },
  navTitle: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: 'white',
  },
  itemPrice: {
    position: 'absolute',
    right: 10,
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 20,
  },
  navBar: {
    paddingVertical: 10,
    backgroundColor: 'deepskyblue',
  },
  btnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  line: {flex: 1, height: 1},
  txtInput: {height: 40, borderBottomWidth: StyleSheet.hairlineWidth},
  txtOr: {alignSelf: 'center'},
  txtOrContainer: {alignItems: 'center', padding: 10},
});

export default index;
