import React, {useEffect, createContext, useState} from 'react';
import AppNavigation from './src/screens/Routes/Routes';
import firebase from '@react-native-firebase/app';
import AsyncStorage from '@react-native-async-storage/async-storage';
import OneSignal from 'react-native-onesignal';
import codePush from 'react-native-code-push';
import {Alert} from 'react-native';
export const RootContext = createContext();

var firebaseConfig = {
  apiKey: 'AIzaSyDNyOF2kJWu5mflSGwZvuhWpKEiS_sxgCU',
  authDomain: 'shaped-network-202507.firebaseapp.com',
  databaseURL: 'https://shaped-network-202507-default-rtdb.firebaseio.com',
  projectId: 'shaped-network-202507',
  storageBucket: 'shaped-network-202507.appspot.com',
  messagingSenderId: '932606465253',
};

firebase.initializeApp(firebaseConfig);
const App = () => {
  const [state, setstate] = useState({
    name: '',
    email: '',
    image: null,
  });

  const [loginType, setLoginType] = useState('default');

  const setUserData = (name, email, image) => {
    setstate({...state, name, email, image});
  };

  const onsetLoginType = async (type) => {
    setLoginType(type);
    try {
      await AsyncStorage.setItem('loginType', type);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    OneSignal.setLogLevel(6, 0);

    OneSignal.init('bc47884b-c31f-4913-8f9e-6f8e75314822', {
      kOSSettingsKeyAutoPrompt: false,
      kOSSettingsKeyInAppLaunchURL: false,
      kOSSettingsKeyInFocusDisplayOption: 2,
    });
    OneSignal.inFocusDisplaying(2);
    OneSignal.addEventListener('received', onReceived);
    OneSignal.addEventListener('opened', onOpened);
    OneSignal.addEventListener('ids', onIds);

    codePush.sync(
      {
        updateDialog: true,
        installMode: codePush.InstallMode.IMMEDIATE,
      },
      SyncStatus,
    );
    return () => {
      OneSignal.removeEventListener('received', onReceived);
      OneSignal.removeEventListener('opened', onOpened);
      OneSignal.removeEventListener('ids', onIds);
    };
  }, []);

  const SyncStatus = (status) => {
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        HTMLFormControlsCollection.log('CHECKING_FOR_UPDATE');
        break;
      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        HTMLFormControlsCollection.log('DOWNLOADING_PACKAGE');
        break;
      case codePush.SyncStatus.UP_TO_DATE:
        HTMLFormControlsCollection.log('UP_TO_DATE');
        break;
      case codePush.SyncStatus.INSTALLING_UPDATE:
        HTMLFormControlsCollection.log('INSTALLING_UPDATE');
        break;
      case codePush.SyncStatus.UPDATE_INSTALLED:
        Alert.alert('Notification', 'Update Installed');
        break;
      case codePush.SyncStatus.AWAITING_USER_ACTION:
        HTMLFormControlsCollection.log('AWAITING_USER_ACTION');
        break;

      default:
        break;
    }
  };

  const onReceived = (notification) => {
    console.log('Notification received: ', notification);
  };

  const onOpened = (openResult) => {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  };

  const onIds = (device) => {
    console.log('Device info: ', device);
  };

  return (
    <RootContext.Provider
      value={{
        state,
        loginType,
        setUserData,
        onsetLoginType,
      }}>
      <AppNavigation />
    </RootContext.Provider>
  );
};

export default App;
