import React, {useState, createContext} from 'react';
import TodoList from './TodoList';
import moment from 'moment';

export const RootContext = createContext();

const Context = () => {
  const [input, setInput] = useState('');
  const [todos, setTodos] = useState([]);

  const handleChangeInput = (value) => {
    setInput(value);
  };

  const addTodo = () => {
    const date = moment().format('DD/MM/YYYY H:mm');
    if (input) {
      setTodos([...todos, {id: moment().unix(), label: input, date}]);
      setInput('');
    }
  };

  const removeTodo = (id) => {
    let todoListTemp = todos.slice();
    todoListTemp.splice(
      todoListTemp.findIndex((i) => {
        return i.id === id;
      }),
      1,
    );
    setTodos(todoListTemp);
  };

  return (
    <RootContext.Provider
      value={{
        input,
        todos,
        handleChangeInput,
        addTodo,
        removeTodo,
      }}>
      <TodoList />
    </RootContext.Provider>
  );
};

export default Context;
